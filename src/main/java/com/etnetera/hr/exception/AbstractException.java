package com.etnetera.hr.exception;

import com.etnetera.hr.controller.dto.ErrorModel;
import com.etnetera.hr.model.enumeration.ErrorCode;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractException extends RuntimeException {

    private final ErrorCode errorCode;
    private final Map<String, String> additionalParams = new HashMap<>();

    public AbstractException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorModel getErrorModel() {
        ErrorModel errorModel = new ErrorModel(errorCode, getMessage());
        errorModel.getAdditionalParams().putAll(additionalParams);
        return errorModel;
    }

    public void withParam(String field, String value) {
        additionalParams.put(field, value);
    }

}
