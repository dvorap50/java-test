package com.etnetera.hr.exception;

import com.etnetera.hr.model.enumeration.ErrorCode;

public class ValidationException extends BusinessException {

    public ValidationException(String field, String value) {
        super(ErrorCode.VALIDATION_FAILED, String.format("Validation failed on field: %s", field));
        withParam(field, value);
    }
}
