package com.etnetera.hr.exception;

import com.etnetera.hr.model.enumeration.ErrorCode;

public class NotFoundException extends AbstractException {

    public NotFoundException(Long id, Class<?> clazz) {
        super(ErrorCode.NOT_FOUND, String.format("Entity %s with id=%s does not found", clazz.getSimpleName(), id));
    }
}
