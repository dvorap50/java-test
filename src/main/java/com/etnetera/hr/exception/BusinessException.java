package com.etnetera.hr.exception;

import com.etnetera.hr.model.enumeration.ErrorCode;

public class BusinessException extends AbstractException {

    public BusinessException(ErrorCode errorCode, String message) {
        super(errorCode, message);
    }
}
