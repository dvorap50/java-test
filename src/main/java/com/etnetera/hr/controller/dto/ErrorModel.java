package com.etnetera.hr.controller.dto;

import com.etnetera.hr.model.enumeration.ErrorCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

@Getter
public class ErrorModel {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final Map<String, String> additionalParams = new HashMap<>();
    private final ErrorCode errorCode;
    private final String message;

    public ErrorModel(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public void withParam(String field, String value) {
        additionalParams.put(field, value);
    }

}
