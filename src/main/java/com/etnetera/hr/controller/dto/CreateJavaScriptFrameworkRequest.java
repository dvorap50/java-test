package com.etnetera.hr.controller.dto;

import com.etnetera.hr.Constants;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

//TODO(dvorakp, 10.09.2022): api dtos should be generated from swagger, using the swagger codegen plugin

@Getter
@Setter
public class CreateJavaScriptFrameworkRequest extends JavaScriptFrameworkVersionDto implements JavaScriptFrameworkInt {

    @NotBlank
    @Size(max = Constants.VALIDATION_MAX_SIZE_NAME)
    private String name;

}
