package com.etnetera.hr.controller.dto;

import com.etnetera.hr.model.JavaScriptFrameworkVersion;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JavaScriptFrameworkVersionDto {

    //Read only
    private Long id;

    @NotBlank
    private String version;

    @NotNull
    private Integer hypeLevel;

    private LocalDate deprecationDate;

    public JavaScriptFrameworkVersionDto(JavaScriptFrameworkVersion javaScriptFrameworkVersion) {
        this.id = javaScriptFrameworkVersion.getId();
        this.version = javaScriptFrameworkVersion.getVersion();
        this.hypeLevel = javaScriptFrameworkVersion.getHypeLevel();
        this.deprecationDate = javaScriptFrameworkVersion.getDeprecationDate();
    }
}
