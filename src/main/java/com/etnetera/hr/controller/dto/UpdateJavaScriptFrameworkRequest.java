package com.etnetera.hr.controller.dto;

import com.etnetera.hr.Constants;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateJavaScriptFrameworkRequest implements JavaScriptFrameworkInt{

    @NotBlank
    @Size(max = Constants.VALIDATION_MAX_SIZE_NAME)
    private String name;

}
