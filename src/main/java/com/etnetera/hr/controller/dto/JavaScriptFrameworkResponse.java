package com.etnetera.hr.controller.dto;

import com.etnetera.hr.model.JavaScriptFramework;
import com.etnetera.hr.util.ConvertUtil;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JavaScriptFrameworkResponse {

    private Long id;
    private String name;
    private List<JavaScriptFrameworkVersionDto> versions;

    public JavaScriptFrameworkResponse(JavaScriptFramework javaScriptFramework) {
        this.id = javaScriptFramework.getId();
        this.name = javaScriptFramework.getName();
        this.versions = ConvertUtil.convertList(javaScriptFramework.getVersions(), JavaScriptFrameworkVersionDto::new);
    }
}
