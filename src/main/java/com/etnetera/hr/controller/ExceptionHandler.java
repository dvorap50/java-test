package com.etnetera.hr.controller;


import com.etnetera.hr.controller.dto.ErrorModel;
import com.etnetera.hr.model.enumeration.ErrorCode;
import com.etnetera.hr.exception.AbstractException;
import com.etnetera.hr.exception.BusinessException;
import com.etnetera.hr.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@ControllerAdvice
public class ExceptionHandler {


    // --- 400 ---
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @org.springframework.web.bind.annotation.ExceptionHandler(NotFoundException.class)
    public ErrorModel handleNotFound(AbstractException e) {
        log.warn("Not found exception", e);
        return e.getErrorModel();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(BusinessException.class)
    public ErrorModel handleBusiness(AbstractException e) {
        log.warn("Business exception", e);
        return e.getErrorModel();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(BindException.class)
    public ErrorModel handleMethodArgumentNotValid(BindException e) {
        log.warn("Bind exception", e);
        return getErrorMessageDto(e.getBindingResult());
    }

    private ErrorModel getErrorMessageDto(BindingResult bindingResult) {
        ErrorModel errorResponse = new ErrorModel(ErrorCode.VALIDATION_FAILED, "Validation failed");
        for (ObjectError error : bindingResult.getAllErrors()) {
            if (error instanceof FieldError) {
                errorResponse.withParam(((FieldError) error).getField(), error.getDefaultMessage());
            }
        }
        return errorResponse;
    }
    //
    // @ResponseStatus(HttpStatus.BAD_REQUEST)
    // @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    // public ErrorMessageDto handleMethodArgumentTypeMismatchException(final MethodArgumentTypeMismatchException e) {
    //     log.warn("Method argument type mismatch exception", e);
    //     return new ErrorMessageDto(ErrorCode.VALIDATION_FAILED, e.getMessage());
    // }
    //
    // @ResponseStatus(HttpStatus.BAD_REQUEST)
    // @ExceptionHandler(MissingServletRequestParameterException.class)
    // public ErrorModel existingApplicationHandler(MissingServletRequestParameterException e) {
    //     log.warn("Missing required parameter", e);
    //     return new ErrorModel(ErrorCode.VALIDATION_FAILED, "Missing required parameter: " + e.getParameterName());
    // }

    // --- 500 ---
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ErrorModel notHandledException(Exception e) {
        log.error("Unknown error", e);
        return new ErrorModel(ErrorCode.UNKNOWN, "Unknown error");
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @org.springframework.web.bind.annotation.ExceptionHandler(AbstractException.class)
    public ErrorModel handleServiceUnavailable(AbstractException e) {
        log.error(e.getMessage(), e);
        return e.getErrorModel();
    }

}
