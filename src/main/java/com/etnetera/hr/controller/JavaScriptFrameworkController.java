package com.etnetera.hr.controller;

import com.etnetera.hr.Constants;
import com.etnetera.hr.controller.dto.CreateJavaScriptFrameworkRequest;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.controller.dto.UpdateJavaScriptFrameworkRequest;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(Constants.PATH_FRAMEWORKS)
public class JavaScriptFrameworkController {

    private final JavaScriptFrameworkService javaScriptFrameworkService;

    @GetMapping
    public Page<JavaScriptFrameworkResponse> find(
            @RequestParam(required = false) String name,
            Pageable pageable) {
        return javaScriptFrameworkService.find(name, pageable);
    }

    @GetMapping("/{id}")
    public JavaScriptFrameworkResponse getOne(@PathVariable Long id) {
        return javaScriptFrameworkService.getFrameWorkResponse(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public JavaScriptFrameworkResponse create(
            @RequestBody @Validated CreateJavaScriptFrameworkRequest request
    ) {
        Long id = javaScriptFrameworkService.create(request);

        return javaScriptFrameworkService.getFrameWorkResponse(id);
    }

    @PostMapping("/{id}/versions")
    public JavaScriptFrameworkResponse addVersion(
            @PathVariable Long id,
            @RequestBody @Validated JavaScriptFrameworkVersionDto request) {
        javaScriptFrameworkService.addVersion(id, request);
        return javaScriptFrameworkService.getFrameWorkResponse(id);
    }

    @PutMapping("/{frameworkId}/versions/{versionId}")
    public JavaScriptFrameworkResponse updateVersion(
            @PathVariable Long frameworkId,
            @PathVariable Long versionId,
            @RequestBody @Validated JavaScriptFrameworkVersionDto request) {
        javaScriptFrameworkService.updateVersion(frameworkId, versionId, request);
        return javaScriptFrameworkService.getFrameWorkResponse(frameworkId);
    }

    @PutMapping("/{id}")
    public JavaScriptFrameworkResponse update(
            @PathVariable Long id,
            @RequestBody @Validated UpdateJavaScriptFrameworkRequest request) {
        javaScriptFrameworkService.update(id, request);
        return javaScriptFrameworkService.getFrameWorkResponse(id);

    }

    @DeleteMapping("/{id}")
    public void remove(
            @PathVariable("id") Long javaScriptFrameworkId) {
        javaScriptFrameworkService.remove(javaScriptFrameworkId);
    }

    @DeleteMapping("/{frameworkId}/versions/{versionId}")
    public JavaScriptFrameworkResponse removeVersion(
            @PathVariable("frameworkId") Long javaScriptFrameworkId,
            @PathVariable("versionId") Long javaScriptFrameworkVersionId) {
        javaScriptFrameworkService.removeVersion(javaScriptFrameworkId, javaScriptFrameworkVersionId);
        return javaScriptFrameworkService.getFrameWorkResponse(javaScriptFrameworkId);
    }

}
