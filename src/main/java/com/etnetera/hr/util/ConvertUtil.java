package com.etnetera.hr.util;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ConvertUtil {

    public <SourceT, TargetT> List<TargetT> convertList(Collection<SourceT> sourceList, Function<SourceT, TargetT> mapperFn) {
        if (sourceList == null) {
            return null;
        }
        return sourceList.stream().map(mapperFn).toList();
    }

}