package com.etnetera.hr;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final int VALIDATION_MAX_SIZE_NAME = 30;
    public static final String PATH_FRAMEWORKS = "/frameworks";

}
