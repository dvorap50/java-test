package com.etnetera.hr.repository;

import com.etnetera.hr.model.GeneralEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
@NoRepositoryBean
public interface GeneralRepository<T extends GeneralEntity> extends PagingAndSortingRepository<T, Long> {
}
