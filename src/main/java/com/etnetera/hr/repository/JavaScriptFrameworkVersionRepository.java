package com.etnetera.hr.repository;

import com.etnetera.hr.model.JavaScriptFrameworkVersion;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface JavaScriptFrameworkVersionRepository extends GeneralRepository<JavaScriptFrameworkVersion> {

    boolean existsByVersionAndJavaScriptFrameworkId(String version, Long javaScriptFrameworkId);

    @Query("SELECT CASE WHEN count(bo) > 0 THEN true ELSE false END FROM JavaScriptFrameworkVersion bo WHERE bo.version = :version AND bo.javaScriptFramework.id = :javaScriptFrameworkId AND bo.id <> :ignoreVersionId")
    boolean existsByFrameWorkWithIgnore(String version, Long javaScriptFrameworkId, Long ignoreVersionId);

}
