package com.etnetera.hr.repository;

import com.etnetera.hr.model.JavaScriptFramework;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface JavaScriptFrameworkRepository extends GeneralRepository<JavaScriptFramework> {

    @Query("FROM JavaScriptFramework bo WHERE bo.name LIKE %:name%")
    Page<JavaScriptFramework> findByNameLike(String name, Pageable pageable);

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, Long ignoreId);
}
