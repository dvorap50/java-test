package com.etnetera.hr.service;

import com.etnetera.hr.controller.dto.CreateJavaScriptFrameworkRequest;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.controller.dto.UpdateJavaScriptFrameworkRequest;
import com.etnetera.hr.model.enumeration.ErrorCode;
import com.etnetera.hr.exception.BusinessException;
import com.etnetera.hr.exception.NotFoundException;
import com.etnetera.hr.model.JavaScriptFramework;
import com.etnetera.hr.model.JavaScriptFrameworkVersion;
import com.etnetera.hr.model.filler.JavaScriptFrameworkFiller;
import com.etnetera.hr.model.filler.JavaScriptFrameworkVersionFiller;
import com.etnetera.hr.exception.ValidationException;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class JavaScriptFrameworkServiceImpl implements JavaScriptFrameworkService {

    private final JavaScriptFrameworkRepository javaScriptFrameworkRepository;
    private final JavaScriptFrameworkVersionRepository javaScriptFrameworkVersionRepository;
    private final JavaScriptFrameworkFiller javaScriptFrameworkFiller;
    private final JavaScriptFrameworkVersionFiller javaScriptFrameworkVersionFiller;

    @Override
    public JavaScriptFrameworkResponse getFrameWorkResponse(Long id) {
        return new JavaScriptFrameworkResponse(getOne(id));
    }

    private JavaScriptFramework getOne(Long id) {
        return javaScriptFrameworkRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id, JavaScriptFramework.class));
    }

    @Override
    public Page<JavaScriptFrameworkResponse> find(String name, Pageable pageable) {
        if (StringUtils.isBlank(name)) {
            return javaScriptFrameworkRepository.findAll(pageable).map(JavaScriptFrameworkResponse::new);
        } else {
            return javaScriptFrameworkRepository.findByNameLike(name, pageable).map(JavaScriptFrameworkResponse::new);
        }
    }

    @Override
    @Transactional
    public Long create(CreateJavaScriptFrameworkRequest request) {
        JavaScriptFramework javaScriptFramework = new JavaScriptFramework();
        if (javaScriptFrameworkRepository.existsByName(request.getName())) {
            throw new ValidationException("name", "not unique");
        }
        javaScriptFrameworkFiller.fill(javaScriptFramework, request);

        JavaScriptFrameworkVersion javaScriptFrameworkVersion = createVersion(request);
        javaScriptFrameworkVersion.setJavaScriptFramework(javaScriptFramework);
        javaScriptFramework.getVersions().add(javaScriptFrameworkVersion);

        JavaScriptFramework saved = javaScriptFrameworkRepository.save(javaScriptFramework);
        log.info("Created new framework [id = {}]", saved.getId());
        return saved.getId();
    }

    @Override
    @Transactional
    public void update(Long id, UpdateJavaScriptFrameworkRequest request) {
        if (javaScriptFrameworkRepository.existsByNameAndIdNot(request.getName(), id)) {
            throw new ValidationException("name", "not unique");
        }
        JavaScriptFramework javaScriptFramework = getOne(id);
        javaScriptFrameworkFiller.fill(javaScriptFramework, request);
        log.info("Updated framework [id = {}]", id);
    }

    @Override
    @Transactional
    public void addVersion(Long javaScriptFrameworkId, JavaScriptFrameworkVersionDto request) {
        if (javaScriptFrameworkVersionRepository.existsByVersionAndJavaScriptFrameworkId(request.getVersion(), javaScriptFrameworkId)) {
            throw new ValidationException("version", "not unique");
        }

        JavaScriptFramework javaScriptFramework = getOne(javaScriptFrameworkId);

        JavaScriptFrameworkVersion frameworkVersion = createVersion(request);
        javaScriptFramework.getVersions().add(frameworkVersion);
        frameworkVersion.setJavaScriptFramework(javaScriptFramework);
        log.info("Added new version to framework [id = {}]", javaScriptFrameworkId);
    }

    @Override
    @Transactional
    public void remove(Long javaScriptFrameworkId) {
        JavaScriptFramework javaScriptFramework = getOne(javaScriptFrameworkId);
        javaScriptFramework.setDeleted(true);
        log.info("Removed framework [id = {}]", javaScriptFrameworkId);
    }

    @Override
    @Transactional
    public void removeVersion(Long javaScriptFrameworkId, Long javaScriptFrameworkVersionId) {
        JavaScriptFrameworkVersion javaScriptFrameworkVersion = getVersion(javaScriptFrameworkVersionId);
        checkVersionBelong(javaScriptFrameworkId, javaScriptFrameworkVersion);
        javaScriptFrameworkVersion.setDeleted(true);
        log.info("Removed version [id={}] from framework [id = {}]", javaScriptFrameworkVersionId, javaScriptFrameworkId);
    }

    @Override
    public void updateVersion(Long frameworkId, Long versionId, JavaScriptFrameworkVersionDto request) {
        JavaScriptFrameworkVersion javaScriptFrameworkVersion = getVersion(versionId);
        checkVersionBelong(frameworkId, javaScriptFrameworkVersion);
        if (javaScriptFrameworkVersionRepository.existsByFrameWorkWithIgnore(request.getVersion(), frameworkId, versionId)) {
            throw new ValidationException("version", "not unique");
        }
        javaScriptFrameworkVersionFiller.fill(javaScriptFrameworkVersion, request);
    }

    private void checkVersionBelong(Long javaScriptFrameworkId, JavaScriptFrameworkVersion javaScriptFrameworkVersion) {
        if (!javaScriptFrameworkVersion.getJavaScriptFramework().getId().equals(javaScriptFrameworkId)) {
            throw new BusinessException(ErrorCode.ID_MISMATCH, "Version not belong to framework");
        }
    }

    private JavaScriptFrameworkVersion getVersion(Long javaScriptFrameworkVersionId) {
        return javaScriptFrameworkVersionRepository.findById(javaScriptFrameworkVersionId)
                .orElseThrow(() -> new NotFoundException(javaScriptFrameworkVersionId, JavaScriptFrameworkVersion.class));
    }

    private JavaScriptFrameworkVersion createVersion(JavaScriptFrameworkVersionDto javaScriptFrameworkVersionRequest) {
        JavaScriptFrameworkVersion javaScriptFrameworkVersion = new JavaScriptFrameworkVersion();
        javaScriptFrameworkVersionFiller.fill(javaScriptFrameworkVersion, javaScriptFrameworkVersionRequest);
        return javaScriptFrameworkVersion;
    }

}
