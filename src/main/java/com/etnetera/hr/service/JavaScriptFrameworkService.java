package com.etnetera.hr.service;

import com.etnetera.hr.controller.dto.CreateJavaScriptFrameworkRequest;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.controller.dto.UpdateJavaScriptFrameworkRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JavaScriptFrameworkService {

    /**
     * Find one specific framework by id.
     *
     * @param id id of framework
     *
     * @return framework
     */
    JavaScriptFrameworkResponse getFrameWorkResponse(Long id);

    /**
     * Find paged frameworks.
     *
     * @param name     name, if null then find all
     * @param pageable pageable settings (size, page, sort...)
     *
     * @return paged frameworks
     */
    Page<JavaScriptFrameworkResponse> find(String name, Pageable pageable);

    /**
     * Create new framework.
     *
     * @param request data
     *
     * @return id of created entity
     */
    Long create(CreateJavaScriptFrameworkRequest request);

    /**
     * Update existng framework base on id.
     *
     * @param id      id of the framework to update
     * @param request data to update
     */
    void update(Long id, UpdateJavaScriptFrameworkRequest request);

    /**
     * Add version to existing framework.
     *
     * @param javaScriptFrameworkId id of the framework to add version into
     * @param request               version data
     */
    void addVersion(Long javaScriptFrameworkId, JavaScriptFrameworkVersionDto request);

    /**
     * Remove framework.
     *
     * @param javaScriptFrameworkId id of the framework to remove
     */
    void remove(Long javaScriptFrameworkId);

    /**
     * Remove version of the framework.
     *
     * @param javaScriptFrameworkId        id of the framework
     * @param javaScriptFrameworkVersionId id of version to remove
     */
    void removeVersion(Long javaScriptFrameworkId, Long javaScriptFrameworkVersionId);

    /**
     * Update version.
     *
     * @param frameworkId id of the framework
     * @param versionId   id of version to update
     * @param request     version data
     */
    void updateVersion(Long frameworkId, Long versionId, JavaScriptFrameworkVersionDto request);
}
