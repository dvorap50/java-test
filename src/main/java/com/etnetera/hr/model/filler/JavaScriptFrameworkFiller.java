package com.etnetera.hr.model.filler;

import com.etnetera.hr.controller.dto.JavaScriptFrameworkInt;
import com.etnetera.hr.model.JavaScriptFramework;
import org.springframework.stereotype.Component;

@Component
public class JavaScriptFrameworkFiller implements GeneralFiller<JavaScriptFramework, JavaScriptFrameworkInt> {

    @Override
    public void fill(JavaScriptFramework target, JavaScriptFrameworkInt source) {
        target.setName(source.getName());
    }
}
