package com.etnetera.hr.model.filler;

public interface GeneralFiller<TargetT, SourceT> {

    void fill(TargetT target, SourceT source);

}
