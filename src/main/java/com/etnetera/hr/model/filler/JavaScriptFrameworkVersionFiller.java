package com.etnetera.hr.model.filler;

import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.model.JavaScriptFrameworkVersion;
import org.springframework.stereotype.Component;

@Component
public class JavaScriptFrameworkVersionFiller implements GeneralFiller<JavaScriptFrameworkVersion, JavaScriptFrameworkVersionDto> {

    @Override
    public void fill(JavaScriptFrameworkVersion target, JavaScriptFrameworkVersionDto source) {
        target.setVersion(source.getVersion());
        target.setHypeLevel(source.getHypeLevel());
        target.setDeprecationDate(source.getDeprecationDate());
    }
}
