package com.etnetera.hr.model.enumeration;

public enum ErrorCode {

    NOT_FOUND,
    UNKNOWN,
    VALIDATION_FAILED,
    ID_MISMATCH

}
