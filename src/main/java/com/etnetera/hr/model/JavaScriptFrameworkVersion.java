package com.etnetera.hr.model;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 *
 * @author Etnetera
 */
@Getter
@Setter
@Entity
@Where(clause = "deleted = false")
public class JavaScriptFrameworkVersion extends GeneralEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private JavaScriptFramework javaScriptFramework;

    private String version;

    private LocalDate deprecationDate;

    private int hypeLevel;

}
