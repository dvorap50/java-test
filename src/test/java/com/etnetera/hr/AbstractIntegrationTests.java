package com.etnetera.hr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTests {

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @LocalServerPort
    protected Integer port;

    protected <ResponseT> ResponseEntity<ResponseT> get(String path, Class<ResponseT> responseClass, Object... pathVars) {
        return testRestTemplate.exchange(getUrl(path), HttpMethod.GET, HttpEntity.EMPTY, responseClass, pathVars);
    }

    protected <RequestT, ResponseT> ResponseEntity<ResponseT> post(String path, RequestT request, Class<ResponseT> responseClass, Object... pathVars) {
        return testRestTemplate.exchange(getUrl(path), HttpMethod.POST, new HttpEntity<>(request), responseClass, pathVars);
    }

    protected <RequestT, ResponseT> ResponseEntity<ResponseT> put(String path, RequestT request, Class<ResponseT> responseClass, Object... pathVars) {
        return testRestTemplate.exchange(getUrl(path), HttpMethod.PUT, new HttpEntity<>(request), responseClass, pathVars);
    }

    protected <ResponseT> ResponseEntity<ResponseT> delete(String path, Class<ResponseT> responseClass, Object... pathVars) {
        return testRestTemplate.exchange(getUrl(path), HttpMethod.DELETE, HttpEntity.EMPTY, responseClass, pathVars);
    }

    protected <ResponseT> ResponseT checkResponseStatus(ResponseEntity<ResponseT> response, HttpStatus httpStatus) {
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(httpStatus, response.getStatusCode());
        return response.getBody();
    }

    private String getUrl(String path) {
        return "http://localhost:" + port + path;
    }

}
