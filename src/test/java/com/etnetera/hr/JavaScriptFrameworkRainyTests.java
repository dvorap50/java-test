package com.etnetera.hr;

import com.etnetera.hr.controller.dto.CreateJavaScriptFrameworkRequest;
import com.etnetera.hr.controller.dto.ErrorModel;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.model.enumeration.ErrorCode;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JavaScriptFrameworkRainyTests extends AbstractIntegrationTests {

    private static final int TEST_HYPE_LEVEL = 3;
    private static final String TEST_NAME = "test_rainy";
    private static final String TEST_NAME_UPDATED = "updated";
    private static final String TEST_VERSION = "v10";
    private static final String TEST_VERSION_2 = "v20";
    private Long frameworkId;
    private Long versionId;

    @Test
    @Order(100)
    public void createTest_notUnique() {
        ResponseEntity<JavaScriptFrameworkResponse> response = post(Constants.PATH_FRAMEWORKS, createRequest(), JavaScriptFrameworkResponse.class);
        ResponseEntity<ErrorModel> errorResponse = post(Constants.PATH_FRAMEWORKS, createRequest(), ErrorModel.class);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.CREATED);
        frameworkId = body.getId();

        ErrorModel errorModel = checkResponseStatus(errorResponse, HttpStatus.BAD_REQUEST);
        checkErrorResponse(errorModel, ErrorCode.VALIDATION_FAILED, true);
    }

    @Test
    @Order(101)
    public void getTest_notFound() {
        ResponseEntity<ErrorModel> errorResponse = get(Constants.PATH_FRAMEWORKS + "/{id}", ErrorModel.class, 100);

        ErrorModel errorModel = checkResponseStatus(errorResponse, HttpStatus.NOT_FOUND);
        checkErrorResponse(errorModel, ErrorCode.NOT_FOUND, false);
    }

    @Test
    @Order(102)
    public void addVersionTest_notUnique() {
        ResponseEntity<JavaScriptFrameworkResponse> response = post(Constants.PATH_FRAMEWORKS + "/{id}/versions", createVersionDto(), JavaScriptFrameworkResponse.class, frameworkId);
        ResponseEntity<ErrorModel> errorResponse = post(Constants.PATH_FRAMEWORKS + "/{id}/versions", createVersionDto(), ErrorModel.class, frameworkId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Optional<JavaScriptFrameworkVersionDto> versionDtoOpt = body.getVersions().stream().filter(dto -> dto.getVersion().equals(TEST_VERSION_2)).findAny();
        Assertions.assertTrue(versionDtoOpt.isPresent());
        versionDtoOpt.ifPresent(dto -> versionId = dto.getId());

        ErrorModel errorModel = checkResponseStatus(errorResponse, HttpStatus.BAD_REQUEST);
        checkErrorResponse(errorModel, ErrorCode.VALIDATION_FAILED, true);
    }

    @Test
    @Order(103)
    public void updateVersionTest_notBelong() {
        ResponseEntity<ErrorModel> response = put(Constants.PATH_FRAMEWORKS + "/{frameworkId}/versions/{versioniId}", createVersionDto(), ErrorModel.class, 500, versionId);

        ErrorModel errorModel = checkResponseStatus(response, HttpStatus.BAD_REQUEST);
        checkErrorResponse(errorModel, ErrorCode.ID_MISMATCH, false);
    }

    private void checkErrorResponse(ErrorModel errorModel, ErrorCode errorCode, boolean hasAdditionalParams) {
        Assertions.assertNotNull(errorModel.getErrorCode());
        Assertions.assertNotNull(errorModel.getMessage());
        Assertions.assertEquals(errorCode, errorModel.getErrorCode());
        if (hasAdditionalParams) {
            Assertions.assertFalse(errorModel.getAdditionalParams().isEmpty());
        } else {
            Assertions.assertTrue(errorModel.getAdditionalParams().isEmpty());
        }
    }

    private CreateJavaScriptFrameworkRequest createRequest() {
        CreateJavaScriptFrameworkRequest createJavaScriptFrameworkRequest = new CreateJavaScriptFrameworkRequest();
        createJavaScriptFrameworkRequest.setName(TEST_NAME);
        createJavaScriptFrameworkRequest.setVersion(TEST_VERSION);
        createJavaScriptFrameworkRequest.setHypeLevel(TEST_HYPE_LEVEL);
        return createJavaScriptFrameworkRequest;
    }

    private JavaScriptFrameworkVersionDto createVersionDto() {
        JavaScriptFrameworkVersionDto dto = new JavaScriptFrameworkVersionDto();
        dto.setVersion(TEST_VERSION_2);
        dto.setHypeLevel(TEST_HYPE_LEVEL);
        return dto;
    }

}
