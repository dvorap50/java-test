package com.etnetera.hr;

import com.etnetera.hr.controller.dto.CreateJavaScriptFrameworkRequest;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import com.etnetera.hr.controller.dto.JavaScriptFrameworkVersionDto;
import com.etnetera.hr.controller.dto.UpdateJavaScriptFrameworkRequest;
import com.etnetera.hr.dto.JavaScriptFrameworkPageResponse;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JavaScriptFrameworkHappyTests extends AbstractIntegrationTests {

    private static final int TEST_HYPE_LEVEL = 3;
    private static final String TEST_NAME = "test";
    private static final String TEST_NAME_UPDATED = "updated";
    private static final String TEST_VERSION = "v1";
    private static final String TEST_VERSION_2 = "v2";
    private Long frameworkId;
    private Long versionId;

    @Test
    @Order(1)
    public void createTest() {
        ResponseEntity<JavaScriptFrameworkResponse> response = post(Constants.PATH_FRAMEWORKS, createRequest(), JavaScriptFrameworkResponse.class);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.CREATED);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(TEST_NAME, body.getName());
        Assertions.assertTrue(body.getVersions().stream().map(JavaScriptFrameworkVersionDto::getVersion).anyMatch(TEST_VERSION::equals));
        frameworkId = body.getId();
    }


    @Test
    @Order(2)
    public void getTest() {
        ResponseEntity<JavaScriptFrameworkResponse> response = get(Constants.PATH_FRAMEWORKS + "/{id}", JavaScriptFrameworkResponse.class, frameworkId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(TEST_NAME, body.getName());
    }

    @Test
    @Order(3)
    public void findTest() {
        ResponseEntity<JavaScriptFrameworkPageResponse> response = get(Constants.PATH_FRAMEWORKS + "?name=es", JavaScriptFrameworkPageResponse.class);

        JavaScriptFrameworkPageResponse body = checkResponseStatus(response, HttpStatus.OK);

        JavaScriptFrameworkResponse javaScriptFrameworkResponse = body.getContent().stream().findAny().orElseThrow();
        Assertions.assertNotNull(javaScriptFrameworkResponse.getId());
        Assertions.assertEquals(TEST_NAME, javaScriptFrameworkResponse.getName());
    }

    @Test
    @Order(4)
    public void addVersionTest() {
        ResponseEntity<JavaScriptFrameworkResponse> response = post(Constants.PATH_FRAMEWORKS + "/{id}/versions", createVersionDto(), JavaScriptFrameworkResponse.class, frameworkId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(TEST_NAME, body.getName());
        Assertions.assertEquals(2, body.getVersions().size());
        Optional<JavaScriptFrameworkVersionDto> versionDtoOpt = body.getVersions().stream().filter(dto -> dto.getVersion().equals(TEST_VERSION_2)).findAny();
        Assertions.assertTrue(versionDtoOpt.isPresent());

        versionDtoOpt.ifPresent(dto -> versionId = dto.getId());
    }

    @Test
    @Order(5)
    public void updateVersionTest() {
        JavaScriptFrameworkVersionDto versionDto = createVersionDto();
        versionDto.setDeprecationDate(LocalDate.now());
        ResponseEntity<JavaScriptFrameworkResponse> response = put(Constants.PATH_FRAMEWORKS + "/{frameworkId}/versions/{versionId}", versionDto, JavaScriptFrameworkResponse.class, frameworkId, versionId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(2, body.getVersions().size());
        Assertions.assertTrue(body.getVersions().stream().map(JavaScriptFrameworkVersionDto::getDeprecationDate).anyMatch(localDate -> LocalDate.now().equals(localDate)));
    }

    @Test
    @Order(6)
    public void updateFrameworkTest() {
        UpdateJavaScriptFrameworkRequest request = new UpdateJavaScriptFrameworkRequest();
        request.setName(TEST_NAME_UPDATED);
        ResponseEntity<JavaScriptFrameworkResponse> response = put(Constants.PATH_FRAMEWORKS + "/{id}", request, JavaScriptFrameworkResponse.class, frameworkId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(TEST_NAME_UPDATED, body.getName());
    }

    @Test
    @Order(7)
    public void removeVersionTest() {
        ResponseEntity<JavaScriptFrameworkResponse> response = delete(Constants.PATH_FRAMEWORKS + "/{frameworkId}/versions/{versionId}", JavaScriptFrameworkResponse.class, frameworkId, versionId);

        JavaScriptFrameworkResponse body = checkResponseStatus(response, HttpStatus.OK);

        Assertions.assertNotNull(body.getId());
        Assertions.assertEquals(1, body.getVersions().size());
    }

    @Test
    @Order(8)
    public void removeFrameworkTest() {
        delete(Constants.PATH_FRAMEWORKS + "/{id}", Void.class, frameworkId);
        ResponseEntity<JavaScriptFrameworkPageResponse> getResponse = get(Constants.PATH_FRAMEWORKS, JavaScriptFrameworkPageResponse.class);

        JavaScriptFrameworkPageResponse body = checkResponseStatus(getResponse, HttpStatus.OK);

        Assertions.assertEquals(0, body.getContent().size());
    }


    private CreateJavaScriptFrameworkRequest createRequest() {
        CreateJavaScriptFrameworkRequest createJavaScriptFrameworkRequest = new CreateJavaScriptFrameworkRequest();
        createJavaScriptFrameworkRequest.setName(TEST_NAME);
        createJavaScriptFrameworkRequest.setVersion(TEST_VERSION);
        createJavaScriptFrameworkRequest.setHypeLevel(TEST_HYPE_LEVEL);
        return createJavaScriptFrameworkRequest;
    }

    private JavaScriptFrameworkVersionDto createVersionDto() {
        JavaScriptFrameworkVersionDto dto = new JavaScriptFrameworkVersionDto();
        dto.setVersion(TEST_VERSION_2);
        dto.setHypeLevel(TEST_HYPE_LEVEL);
        return dto;
    }
}
