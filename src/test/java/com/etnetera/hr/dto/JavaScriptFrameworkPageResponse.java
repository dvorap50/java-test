package com.etnetera.hr.dto;

import com.etnetera.hr.controller.dto.JavaScriptFrameworkResponse;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JavaScriptFrameworkPageResponse {

    private List<JavaScriptFrameworkResponse> content;

}
